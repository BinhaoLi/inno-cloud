import { createStore as reduxCreateStore } from "redux";

const RESET = `RESET`
const CHANGE = `CHANGE`
const SELECTITEM = `SELECTITEM`
const RESETITEM = `RESETITEM`

const reducer = (state, action) => {
  switch(action.type){
        case RESET:
          return Object.assign({}, state, {
            open: null,
          })
        case CHANGE:
          const {to} = action.payload;
          return Object.assign({}, state, {
            open: to,
          })
        case SELECTITEM:
          const {item} = action.payload;
          return Object.assign({}, state, {
            selectedItem: item,
          })
        case RESETITEM:
          return Object.assign({}, state, {
            selectedItem: null,
          })
        default:
          return state
    }
}

const initialState = {
   open: null,
   selectedItem: null,
 }

const createStore = () => reduxCreateStore(reducer, initialState)
export default createStore
