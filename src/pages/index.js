import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout"
import InteractiveList from "../components/interactiveList"

export default ({ data }) => {
  return (
    <div>
      <Layout>
        <InteractiveList nodes={data.allMarkdownRemark.edges}/>
      </Layout>
    </div>
  )
}

export const query = graphql`
  query {
    allMarkdownRemark(sort: { fields: [frontmatter___title], order: ASC }) {
      totalCount
      edges {
        node {
          id
          frontmatter {
            title
            catalog
          }
          fields {
            slug
          }
          excerpt
        }
      }
    }
  }
`
