import React from "react";
import PropTypes from "prop-types";
import { css } from "@emotion/core";
import { Link } from "gatsby";
import { connect } from "react-redux";
import AppBar from "@material-ui/core/AppBar";
import CssBaseline from "@material-ui/core/CssBaseline";
import Divider from "@material-ui/core/Divider";
import Drawer from "@material-ui/core/Drawer";
import Hidden from "@material-ui/core/Hidden";
import IconButton from "@material-ui/core/IconButton";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import MenuIcon from "@material-ui/icons/Menu";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import Collapse from "@material-ui/core/Collapse";
import FolderIcon from "@material-ui/icons/Folder";
import FolderOpenIcon from "@material-ui/icons/FolderOpen";

const drawerWidth = 240;

const styles = theme => ({
  root: {
    display: "flex"
  },
  drawer: {
    [theme.breakpoints.up("sm")]: {
      width: drawerWidth,
      flexShrink: 0
    }
  },
  appBar: {
    marginLeft: drawerWidth,
    [theme.breakpoints.up("sm")]: {
      width: `calc(100% - ${drawerWidth}px)`
    }
  },
  menuButton: {
    marginRight: 20,
    [theme.breakpoints.up("sm")]: {
      display: "none"
    }
  },
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3
  }
});

class ResponsiveDrawer extends React.Component {
  state = {
    mobileOpen: false
  };

  handleDrawerToggle = () => {
    this.setState(state => ({ mobileOpen: !state.mobileOpen }));
  };

  handleClick = (e, group, open) => {
    if (open === group.fieldValue) {
      this.props.reset();
    } else {
      const to = group.fieldValue;
      this.props.change({ to });
    }
  };

  handleSelect = (e, node) => {
    const item = node.frontmatter.title;
    this.props.selectItem({ item });
  };

  resetItem = () => {
    this.props.resetItem();
  }

  render() {
    const { classes, theme, children, data, open, selectedItem } = this.props;

    const drawer = (
      <div>
        <div className={classes.toolbar} />
        <Divider />
        <List>
          {data.allMarkdownRemark.group.map(group => (
            <div key={group.fieldValue}>
              <ListItem button onClick={e => this.handleClick(e, group, open)}>
                <ListItemIcon>
                  {open === group.fieldValue ? <FolderOpenIcon /> : <FolderIcon />}
                </ListItemIcon>
                <ListItemText primary={group.fieldValue} />
                {open === group.fieldValue ? <ExpandLess /> : <ExpandMore />}
              </ListItem>
              <Collapse
                in={open === group.fieldValue}
                timeout="auto"
                unmountOnExit
              >
                <List component="div" disablePadding>
                  {group.edges.map(({ node }) => (
                    <Link
                      to={node.fields.slug}
                      key={node.id}
                      css={css`
                        text-decoration: none;
                        color: inherit;
                      `}
                    >
                      <ListItem
                        button
                        selected={node.frontmatter.title === selectedItem}
                        onClick={e => this.handleSelect(e, node)}
                        disableRipple
                        className={classes.nested}
                      >
                        <ListItemText inset primary={node.frontmatter.title} />
                      </ListItem>
                    </Link>
                  ))}
                </List>
              </Collapse>
            </div>
          ))}
        </List>
      </div>
    );

    return (
      <div className={classes.root}>
        <CssBaseline />
        <AppBar position="fixed" className={classes.appBar}>
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="Open drawer"
              onClick={this.handleDrawerToggle}
              className={classes.menuButton}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" color="inherit" noWrap>
              <Link
                to="/"
                onClick={this.resetItem}
                css={css`
                  text-decoration: none;
                  color: inherit;
                `}
              >
                校园VC加速营
              </Link>
            </Typography>
          </Toolbar>
        </AppBar>
        <nav className={classes.drawer}>
          {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
          <Hidden smUp implementation="css">
            <Drawer
              container={this.props.container}
              variant="temporary"
              anchor={theme.direction === "rtl" ? "right" : "left"}
              open={this.state.mobileOpen}
              onClose={this.handleDrawerToggle}
              classes={{
                paper: classes.drawerPaper
              }}
            >
              {drawer}
            </Drawer>
          </Hidden>
          <Hidden xsDown implementation="css">
            <Drawer
              classes={{
                paper: classes.drawerPaper
              }}
              variant="permanent"
              open
            >
              {drawer}
            </Drawer>
          </Hidden>
        </nav>
        <main className={classes.content}>
          <div className={classes.toolbar} />
          {children}
        </main>
      </div>
    );
  }
}

ResponsiveDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
  // Injected by the documentation to work in an iframe.
  // You won't need it on your project.
  container: PropTypes.object,
  theme: PropTypes.object.isRequired
};

const mapStateToProps = ({ open,  selectedItem}) => {
  return { open, selectedItem };
};

const mapDispatchToProps = dispatch => {
  return {
    reset: () => dispatch({ type: `RESET` }),
    change: ({ to }) => dispatch({ type: `CHANGE`, payload: { to } }),
    selectItem: ({ item }) => dispatch({ type: `SELECTITEM`, payload: { item } }),
    resetItem: () => dispatch({ type: `RESETITEM`}),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles, { withTheme: true })(ResponsiveDrawer));
