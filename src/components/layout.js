import React from "react";
import { StaticQuery, graphql } from "gatsby";

import ResponsiveDrawer from "./responsiveDrawer";

export default ({ children }) => (
  <StaticQuery
    query={graphql`
      query {
        allMarkdownRemark(sort: { fields: [frontmatter___title], order: ASC }) {
          group(field: frontmatter___catalog) {
            fieldValue
            totalCount
            edges {
              node {
                id
                frontmatter {
                  title
                }
                fields {
                  slug
                }
                excerpt
              }
            }
          }
        }
      }
    `}
    render={data => (
      <ResponsiveDrawer children={children} data={data} />
    )}
  />
);
