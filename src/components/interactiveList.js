import React from "react";
import PropTypes from "prop-types";
import { css } from "@emotion/core"
import { Link } from "gatsby"
import { withStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemText from "@material-ui/core/ListItemText";
import Avatar from "@material-ui/core/Avatar";
import FolderIcon from "@material-ui/icons/Folder";
import { connect } from "react-redux";

const styles = theme => ({
  root: {
    flexGrow: 1
  },
  demo: {
    backgroundColor: theme.palette.background.paper
  },
  title: {
    margin: `${theme.spacing.unit * 4}px 0 ${theme.spacing.unit * 2}px`
  }
});

class InteractiveList extends React.Component {
  handleSelect = (e, node) => {
    const item = node.frontmatter.title;
    const to = node.frontmatter.catalog;
    this.props.selectItem({ item });
    this.props.change({ to });
  };

  render() {
    const { classes, nodes, resetItem } = this.props;

    return (
      <div className={classes.root}>
        <div className={classes.demo}>
          <List>
            {nodes.map(({ node }) => (
              <Link
                to={node.fields.slug}
                key={node.id}
                css={css`
                  text-decoration: none;
                  color: inherit;
                  `}
                  >
              <ListItem button onClick={e => this.handleSelect(e, node)} disableRipple key={node.id}>
                <ListItemAvatar>
                    <Avatar>
                      <FolderIcon />
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText
                    primary={node.frontmatter.title}
                    secondary={node.excerpt}
                  />
              </ListItem>
            </Link>
            ))}
          </List>
        </div>
      </div>
    );
  }
}

InteractiveList.propTypes = {
  classes: PropTypes.object.isRequired
};

  const mapStateToProps = ({selectedItem}) => {
    return { selectedItem };
  };

const mapDispatchToProps = dispatch => {
  return {
    change: ({ to }) => dispatch({ type: `CHANGE`, payload: { to } }),
    selectItem: ({ item }) => dispatch({ type: `SELECTITEM`, payload: { item } }),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(InteractiveList));
